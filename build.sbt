lazy val core = (project in file("core")).settings(
  organization := "tf.bug",
  name := "complexmap",
  version := "0.1.0",
  scalaVersion := "2.13.4",
  resolvers += "jitpack" at "https://jitpack.io",
  libraryDependencies ++= Seq(
    "com.github.outwatch.outwatch" %%% "outwatch" % "master-SNAPSHOT",
    "org.typelevel" %%% "cats-parse" % "0.1.0",
    "co.fs2" %%% "fs2-core" % "2.4.4",
    "org.typelevel" %%% "spire" % "0.17.0",
  ),
  Compile / npmDependencies ++= Seq(
    "mathlive" -> "0.59.0",
    "snabbdom" -> "git://github.com/outwatch/snabbdom.git#semver:0.7.5",
  ),
  scalaJSUseMainModuleInitializer := true,
  useYarn := true,
  webpackBundlingMode := BundlingMode.Application,
  scalaJSLinkerConfig ~= (_.withModuleKind(ModuleKind.CommonJSModule)),
).enablePlugins(ScalablyTypedConverterPlugin)
