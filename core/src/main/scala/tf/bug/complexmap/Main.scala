package tf.bug.complexmap

import cats.effect._
import cats.implicits._
import fs2._
import org.scalajs.dom
import typings.mathlive.{mod => mathlive}

object Main extends IOApp {
  
  override def run(args: List[String]): IO[ExitCode] = {
    program[IO].compile.drain.as(ExitCode.Success)
  }

  val inputCanvasId = "input-canvas"
  val outputCanvasId = "output-canvas"
  val expressionBoxId = "expression-box"

  def program[F[_]: Sync]: Stream[F, Unit] = {
    import outwatch._
    import outwatch.dsl._

    val inputCanvas = canvas(widthAttr := 512, heightAttr := 512, idAttr := inputCanvasId)
    val outputCanvas = canvas(widthAttr := 512, heightAttr := 512, idAttr := outputCanvasId)

    val expressionBox = new mathlive.MathfieldElement()
    val expressionBoxVNodeProxy = snabbdom.tovnode(expressionBox.asInstanceOf[dom.Element])
    val expressionBoxVNode = VDomModifier(VNodeProxyNode(expressionBoxVNodeProxy), onDomMount.foreach { elem =>
      val input = elem.getElementsByTagName("math-field")(0).asInstanceOf[mathlive.MathfieldElement]
      input.id_=(expressionBoxId)
      input.value_=("f(z) = z^2")
    })

    val container = div(inputCanvas, outputCanvas, expressionBoxVNode, idAttr := "app")

    val render = OutWatch.renderReplace[F]("#app", container)

    val inputCanvasElem: F[dom.html.Canvas] = getInputCanvas[F]
    val outputCanvasElem: F[dom.html.Canvas] = getOutputCanvas[F]

    val inputCanvasWebGL: F[WebGL[F]] = inputCanvasElem >>= WebGL.apply[F]
    val outputCanvasWebGL: F[WebGL[F]] = outputCanvasElem >>= WebGL.apply[F]

    val clearInputCanvas = inputCanvasWebGL.flatMap { webGl =>
      webGl.clearColor(1.0D, 0.0D, 0.0D, 1.0D) >> webGl.clear(dom.webgl.RenderingContext.COLOR_BUFFER_BIT)
    }
    val clearOutputCanvas = outputCanvasWebGL.flatMap { webGl =>
      webGl.clearColor(0.0D, 1.0D, 0.0D, 1.0D) >> webGl.clear(dom.webgl.RenderingContext.COLOR_BUFFER_BIT)
    }

    Stream.eval(render >> clearInputCanvas >> clearOutputCanvas)
  }

  def getInputCanvas[F[_]: Sync]: F[dom.html.Canvas] = {
    Sync[F].delay(dom.document.getElementById(inputCanvasId).asInstanceOf[dom.html.Canvas])
  }

  def getOutputCanvas[F[_]: Sync]: F[dom.html.Canvas] = {
    Sync[F].delay(dom.document.getElementById(outputCanvasId).asInstanceOf[dom.html.Canvas])
  }

}
