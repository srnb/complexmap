package tf.bug.complexmap

import cats.effect._
import cats.implicits._
import org.scalajs.dom

trait WebGL[F[_]] {
  
  def clearColor(red: Double, green: Double, blue: Double, alpha: Double): F[Unit]

  def clear(mask: Int): F[Unit]

}

object WebGL {

  def apply[F[_]](elem: dom.html.Canvas)(implicit F: Sync[F]): F[WebGL[F]] =
    F.delay(elem.getContext("webgl").asInstanceOf[dom.webgl.RenderingContext]).map(apply[F])

  def apply[F[_]](ctx: dom.webgl.RenderingContext)(implicit F: Sync[F]): WebGL[F] = new WebGL[F] {

    override def clearColor(red: Double, green: Double, blue: Double, alpha: Double): F[Unit] =
      F.delay(ctx.clearColor(red, green, blue, alpha))

    override def clear(mask: Int): F[Unit] =
      F.delay(ctx.clear(mask))

  }

}
