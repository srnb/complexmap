# complexmap

A web application to visualize how complex functions affect a plane.

## run

Currently, to test the application:
1. Start an `sbt shell` with at least 2GiB of memory
2. `core/fastOptJS::startWebpackDevServer` to start the local server
3. `~core/fastOptJS` to start watching and compiling the source code

When you're done:
1. Press Enter to stop watching the source code
2. `core/fastOptJS::stopWebpackDevServer` to stop the local server
3. `exit` to exit the SBT shell
