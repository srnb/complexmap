addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat" % "0.1.15")

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.3.1")
addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.20.0")

resolvers += Resolver.bintrayRepo("oyvindberg", "converter")
resolvers += Resolver.bintrayRepo("oyvindberg", "converter-snapshots")
addSbtPlugin("org.scalablytyped.converter" % "sbt-converter" % "1.0.0-beta29+8-8214a39e")

addSbtPlugin("org.jmotor.sbt" % "sbt-dependency-updates" % "1.2.2")
